<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Projeto extends CI_Model{
	private $nome;
	private $cliente;
	private $custo;
	private $dataInicio;
	private $status;
	private $etapas;
	private $pagamentos;
	private $recursos;
	private $funcionarios;


	public function __construct(){
		parent::__construct();
	}

	public function exibirGraficos(){

	}

	public function retornarProgresso(){
		
	}

	public function calcularCusto(){

	}

	public function realizarPagamento($tipo,$valor){

	}

	public function adicionarCusto(){
		
	}

	public function buscarProjetoAtivo($cliente){

	}
	
}